@extends('layouts.main')

@section('content')
    <weather-component cities="{{ json_encode($cities) }}"></weather-component>
    </div>
@endsection
