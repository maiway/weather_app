<?php

use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cities')->insert([
            'name' => 'Tokyo',
            'country' => 'jp',
            'description' => 'Tokyo, Japan’s busy capital, mixes the ultramodern and the traditional, from neon-lit skyscrapers to historic temples. The opulent Meiji Shinto Shrine is known for its towering gate and surrounding woods. The Imperial Palace sits amid large public ga',
            'source' => 'https://www.japan-guide.com/e/e2164.html',
        ]);

        DB::table('cities')->insert([
            'name' => 'Yokohama',
            'country' => 'jp',
            'description' => 'Yokohama (横浜) is Japan\'s second largest city with a population of over three million. Yokohama is located less than half an hour south of Tokyo by train, and is the capital of Kanagawa Prefecture.',
            'source' => 'https://www.japan-guide.com/e/e2156.html',
        ]);

        DB::table('cities')->insert([
            'name' => 'Kyoto',
            'country' => 'jp',
            'description' => 'Kyoto (京都, Kyōto) served as Japan\'s capital and the emperor\'s residence from 794 until 1868. It is one of the country\'s ten largest cities with a population of 1.5 million people and a modern face.',
            'source' => 'https://www.japan-guide.com/e/e2158.html',
        ]);

        DB::table('cities')->insert([
            'name' => 'Osaka',
            'country' => 'jp',
            'description' => 'Osaka is only a short shinkansen ride from Tokyo, but has a very different personality to Japan\'s capital city. Hop off the bullet train into an area of exciting nightlife, delicious food and straight-talking, friendly locals.',
            'source' => 'https://www.japan.travel/en/destinations/kansai/osaka/',
        ]);

        DB::table('cities')->insert([
            'name' => 'Sapporo',
            'country' => 'jp',
            'description' => 'Sapporo (札幌, "important river flowing through a plain" in Ainu language) is the capital of Hokkaido and Japan\'s fifth largest city. Sapporo is also one of the nation\'s youngest major cities. In 1857, the city\'s population stood at just seven people.',
            'source' => 'https://www.japan-guide.com/e/e2163.html',
        ]);

        DB::table('cities')->insert([
            'name' => 'Nagoya',
            'country' => 'jp',
            'description' => 'With over two million inhabitants, Nagoya (名古屋) is Japan\'s fourth most populated city. It is the capital of Aichi Prefecture and the principal city of the Nobi plain, one of Honshu\'s three large plains and metropolitan and industrial centers.',
            'source' => 'https://www.japan-guide.com/e/e2155.html',
        ]);
    }
}
