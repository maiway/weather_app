## About weather_app

Provides basic travel information of Japan for foreign tourists visiting Japan for the first time with the following cities.
Tokyo, Yokohama, Kyoto, Osaka, Sapporo, Nagoya

- Tokyo
- Yokohama
- Kyoto
- Osaka
- Sapporo
- Nagoya

## How to install?

- Clone repository
- composer install
- npm install
- edit .env
- php artisan key:generate
- php artisan migrate
- php artisan db:seed --class=CitiesTableSeeder
- create .htacess file in your root directory and placed the following code:

**

    <IfModule mod_rewrite.c>
        <IfModule mod_negotiation.c>
            Options -MultiViews
        </IfModule>
    
        RewriteEngine On

        RewriteCond %{REQUEST_FILENAME} -d [OR]
        RewriteCond %{REQUEST_FILENAME} -f
        RewriteRule ^ ^$1 [N]

        RewriteCond %{REQUEST_URI} (\.\w+$) [NC]
        RewriteRule ^(.*)$ public/$1 

        RewriteCond %{REQUEST_FILENAME} !-d
        RewriteCond %{REQUEST_FILENAME} !-f
        RewriteRule ^ server.php

    </IfModule>
**

- Run http://yourhost/projectfolder
