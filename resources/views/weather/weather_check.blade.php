@extends('layouts.main')

@section('content')
    <div class="container">
        <forecast-component city="{{ $city }}" country="{{ $country }}"></forecast-component>
        <foursquare-component city="{{ $city }}" country="{{ $country }}"></foursquare-component>
    </div>
@endsection