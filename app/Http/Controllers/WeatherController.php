<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WeatherController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $cities = \DB::table('cities')->get();
        return view('weather.view',compact('cities'));
    }

    public function check_weather($city,$country)
    {
    	return view('weather.weather_check',compact('city','country'));
    }
}
